package com.omahtiugm.jakartasehat;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.Point;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AbsoluteLayout;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Row;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static android.view.View.INVISIBLE;
import static android.view.View.OnClickListener;
import static android.view.View.VISIBLE;
import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;

public class MapFragment
        extends
        com.google.android.gms.maps.MapFragment
        implements
        GoogleMap.OnMapClickListener,
        GoogleMap.OnMarkerClickListener,
        OnClickListener, LocationListener, GooglePlayServicesClient.ConnectionCallbacks, GooglePlayServicesClient.OnConnectionFailedListener, LocationProvider.LocationCallback {

    private static ArrayList<Spot> SPOT = new ArrayList<Spot>();
    /*Spot[] SPOTS_ARRAY = new Spot[]{
            new Spot("Киев", new LatLng(50.4546600, 30.5238000)),
            new Spot("Одесса", new LatLng(46.4774700, 30.7326200)),
            new Spot("Харьков", new LatLng(50.0000000, 36.2500000)),
            new Spot("Львов", new LatLng(49.8382600, 24.0232400)),
            new Spot("Донецк", new LatLng(48.0000000, 37.8000000)),
    };*/

    private static final int POPUP_POSITION_REFRESH_INTERVAL = 16;
    //длительность анимации перемещения карты
    private static final int ANIMATION_DURATION = 500;

    private Map<Marker, Spot> spots;

    private LatLng trackedPosition;

    private Handler handler;

    private Runnable positionUpdaterRunnable;
    public static final String TAG = MapFragment.class.getSimpleName();

    private int popupXOffset;
    private int popupYOffset;
    //высота маркера
    private int markerHeight;
    private AbsoluteLayout.LayoutParams overlayLayoutParams;

    Spinner mSprPlaceType;

    String[] mPlaceType = null;
    String[] mPlaceTypeName = null;

    double mLatitude = 0;
    double mLongitude = 0;

    private ArrayList<MyMarker> mMyMarkersArray = new ArrayList<MyMarker>();
    private HashMap<Marker, MyMarker> mMarkersHashMap;
    private ArrayList<String> data = new ArrayList<String>();
    private ArrayList<String> address = new ArrayList<String>();
    private ArrayList<String> telepon = new ArrayList<String>();

    private ViewTreeObserver.OnGlobalLayoutListener infoWindowLayoutListener;

    private View infoWindowContainer;
    private TextView textView;
    private TextView alamat;
    private TextView button;
    GoogleMap map;
    private Location locations;
    private LatLng myLocation;
    double temp_lat = 0;
    double temp_long = 0;
    public static final String MyPREFERENCES = "MyPrefs";
    SharedPreferences sharedpreferences;
    AsyncHttpClient client = new AsyncHttpClient();
    private LocationProvider mLocationProvider;
    private int Index;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUpMapIfNeeded();

        mLocationProvider = new LocationProvider(getActivity(), this);
        spots = new HashMap<>();
        markerHeight = getResources().getDrawable(R.drawable.pin).getIntrinsicHeight();
        sharedpreferences = getActivity().getSharedPreferences(MyPREFERENCES,
                Context.MODE_PRIVATE);

    }

    View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment, null);
        mMarkersHashMap = new HashMap<Marker, MyMarker>();
        asyncdata();
        File file = new File(ExternalStorageDirectoryPath + "/data.xls");
        if (!file.exists()) {
            new DownloadTask(getActivity()).execute("url");
        } else {
            readExcelFileFromAssets();
        }
        //readExcelFileFromAssets();
        // Array of place types
        mPlaceType = getResources().getStringArray(R.array.place_type);

        // Array of place type names
        mPlaceTypeName = getResources().getStringArray(R.array.place_type_name);

        // Creating an array adapter with an array of Place types
        // to populate the spinner
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, mPlaceTypeName);

        // Getting reference to the Spinner
        mSprPlaceType = (Spinner) rootView.findViewById(R.id.spr_place_type);

        // Setting adapter on Spinner to set place types
        mSprPlaceType.setAdapter(adapter);

        com.gc.materialdesign.views.ButtonRectangle btnFind;

        // Getting reference to Find Button
        btnFind = (com.gc.materialdesign.views.ButtonRectangle) rootView.findViewById(R.id.btn_find);
        btnFind.setBackgroundColor(Color.parseColor("#1E88E5"));

        FrameLayout containerMap = (FrameLayout) rootView.findViewById(R.id.container_map);
        View mapView = super.onCreateView(inflater, container, savedInstanceState);
        containerMap.addView(mapView, new FrameLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT));
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity().getBaseContext());


        btnFind.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {


                int selectedPosition = mSprPlaceType.getSelectedItemPosition();
                String type = mPlaceType[selectedPosition];

                if (!type.contains("puskesmas")&&!type.contains("BPJS+Kesehatan+Jakarta")) {
                    StringBuilder sb = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
                    sb.append("location=" + mLatitude + "," + mLongitude);
                    sb.append("&radius=5000");
                    sb.append("&types=" + type);
                    //sb.append("&sensor=true");
                    sb.append("&key=AIzaSyA85niT7OmYt-XB_NsGTh_7G41HzGRuT_U");


                    // Creating a new non-ui thread task to download Google place json data
                    PlacesTask placesTask = new PlacesTask();

                    // Invokes the "doInBackground()" method of the class PlaceTask
                    placesTask.execute(sb.toString());
                }else{
                    StringBuilder sb = new StringBuilder("https://maps.googleapis.com/maps/api/place/textsearch/json?");
                    sb.append("location=" + mLatitude + "," + mLongitude);
                    sb.append("&radius=5000");
                    sb.append("&query=" + type);
                    //sb.append("&sensor=true");
                    sb.append("&key=AIzaSyA85niT7OmYt-XB_NsGTh_7G41HzGRuT_U");


                    // Creating a new non-ui thread task to download Google place json data
                    PlacesTask placesTask = new PlacesTask();

                    // Invokes the "doInBackground()" method of the class PlaceTask
                    placesTask.execute(sb.toString());
                }


            }
        });

        infoWindowContainer = rootView.findViewById(R.id.container_popup);
        //подписываемся на изменения размеров всплывающего окна
        infoWindowLayoutListener = new InfoWindowLayoutListener();
        infoWindowContainer.getViewTreeObserver().addOnGlobalLayoutListener(infoWindowLayoutListener);
        overlayLayoutParams = (AbsoluteLayout.LayoutParams) infoWindowContainer.getLayoutParams();

        textView = (TextView) infoWindowContainer.findViewById(R.id.textview_title);
        button = (TextView) infoWindowContainer.findViewById(R.id.button_view_article);
        button.setOnClickListener(this);


        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        setUpMapIfNeeded();
        mLocationProvider.connect();
    }

    @Override
    public void onPause() {
        super.onPause();
        mLocationProvider.disconnect();
    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (map == null) {
            // Try to obtain the map from the SupportMapFragment.
            map = getMap();
            /*map.setOnMapClickListener(this);
            map.setOnMarkerClickListener(this);*/
            // Check if we were successful in obtaining the map.
            if (map != null) {
                setUpMap();

            }
        }
    }

    private void setUpMap() {
        //map.addMarker(new MarkerOptions().position(new LatLng(0, 0)).title("Marker").snippet("Snippet"));

        // Enable MyLocation Layer of Google Map
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity().getBaseContext());


        if (status != ConnectionResult.SUCCESS) { // Google Play Services are not available

            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, getActivity(), requestCode);
            dialog.show();

        } else {
            map.setMyLocationEnabled(true);
            /*map.getUiSettings().setMyLocationButtonEnabled(false);
            map.getUiSettings().setRotateGesturesEnabled(false);*/
            map.setOnMapClickListener(this);
            map.setOnMarkerClickListener(this);
            map.getUiSettings().setAllGesturesEnabled(true);
            // Get LocationManager object from System Service LOCATION_SERVICE
            LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

            // Create a criteria object to retrieve provider
            Criteria criteria = new Criteria();

            // Get the name of the best provider
            String provider = locationManager.getBestProvider(criteria, true);

            // Get Current Location
            Location myLocation = locationManager.getLastKnownLocation(provider);

            // set map type
            map.setMapType(GoogleMap.MAP_TYPE_NORMAL);

            // Get latitude of the current location
            double latitude = myLocation.getLatitude();

            // Get longitude of the current location
            double longitude = myLocation.getLongitude();

            // Create a LatLng object for the current location
            LatLng latLng = new LatLng(latitude, longitude);
            mLatitude = latLng.latitude;
            mLongitude = latLng.longitude;

            // Show the current location in Google Map
            map.moveCamera(CameraUpdateFactory.newLatLng(latLng));

            // Zoom in the Google Map
            map.animateCamera(CameraUpdateFactory.zoomTo(14));
            //map.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).title("Anda"));
        }
    }

    private LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            if (location == null)
                return;

            if (mLatitude == location.getLatitude() && mLongitude == location.getLongitude()) {
                Log.e("TAG", "location not changed.");
                return;
            }

            mLatitude = location.getLatitude();
            mLongitude = location.getLongitude();
            //Toast.makeText(getActivity().getBaseContext(),String.valueOf(mLatitude),Toast.LENGTH_LONG).show();
            Log.i("TAG", "Location changed to (" + mLatitude + ", " + mLongitude + ")");


        }
    };


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //очистка
        handler = new Handler(Looper.getMainLooper());
        positionUpdaterRunnable = new PositionUpdaterRunnable();
        handler.post(positionUpdaterRunnable);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        infoWindowContainer.getViewTreeObserver().removeGlobalOnLayoutListener(infoWindowLayoutListener);
        handler.removeCallbacks(positionUpdaterRunnable);
        handler = null;
    }

    @Override
    public void onClick(View v) {
        String telp = (String) v.getTag();
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + telp));
        startActivity(callIntent);
    }

    @Override
    public void onMapClick(LatLng latLng) {
        infoWindowContainer.setVisibility(INVISIBLE);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        //GoogleMap map = getMap();
        if (!marker.getTitle().equals("Anda disini")) {
            Projection projection = map.getProjection();
            trackedPosition = marker.getPosition();
            Point trackedPoint = projection.toScreenLocation(trackedPosition);
            //trackedPoint.y -= popupYOffset / ;
            LatLng newCameraLocation = projection.fromScreenLocation(trackedPoint);
            map.animateCamera(CameraUpdateFactory.newLatLng(newCameraLocation), ANIMATION_DURATION, null);

            Spot spot = spots.get(marker);
            textView.setText(spot.getName());
            alamat.setText(spot.getAddress());
            button.setTag(spot.getTelpon());

            infoWindowContainer.setVisibility(VISIBLE);

            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onDisconnected() {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void handleNewLocation(Location location) {
        Log.d(TAG, location.toString());

        double currentLatitude = location.getLatitude();
        double currentLongitude = location.getLongitude();
        mLatitude = currentLatitude;
        mLongitude = currentLongitude;
        LatLng latLng = new LatLng(currentLatitude, currentLongitude);

        //mMap.addMarker(new MarkerOptions().position(new LatLng(currentLatitude, currentLongitude)).title("Current Location"));
        MarkerOptions options = new MarkerOptions()
                .position(latLng)
                .title("Anda disini");
        map.addMarker(options);
        map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        map.animateCamera(CameraUpdateFactory.zoomTo(14));
    }

    private class InfoWindowLayoutListener implements ViewTreeObserver.OnGlobalLayoutListener {
        @Override
        public void onGlobalLayout() {
            //размеры окна изменились, обновляем смещения
            popupXOffset = infoWindowContainer.getWidth() / 2;
            popupYOffset = (int) (infoWindowContainer.getHeight() / 1.5);
        }
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);


            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
                Log.e("Bisa", line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.e("Error", e.getLocalizedMessage());
        } finally {
            Log.e("Error", "Error Download URL");
            iStream.close();
            urlConnection.disconnect();
        }

        return data;
    }

    private class ParserTask extends AsyncTask<String, Integer, List<HashMap<String, String>>> {

        JSONObject jObject;
        int selectedPosition = mSprPlaceType.getSelectedItemPosition();
        String type = mPlaceType[selectedPosition];

        // Invoked by execute() method of this object
        @Override
        protected List<HashMap<String, String>> doInBackground(String... jsonData) {

            List<HashMap<String, String>> places = null;
            int selectedPosition = mSprPlaceType.getSelectedItemPosition();
            String type = mPlaceType[selectedPosition];
            PlaceJSONParser placeJsonParser;

            placeJsonParser = new PlaceJSONParser("0");


            try {
                jObject = new JSONObject(jsonData[0]);

                /** Getting the parsed data as a List construct */
                places = placeJsonParser.parse(jObject);

            } catch (Exception e) {
                Log.e("Error", e.getLocalizedMessage());
            }
            return places;
        }

        // Executed after the complete execution of doInBackground() method
        @Override
        protected void onPostExecute(List<HashMap<String, String>> list) {

            // Clears all the existing markers
            double dist = 999999999;

            map.clear();
            SPOT.clear();
            for (int i = 0; i < list.size(); i++) {

                // Creating a marker
                MarkerOptions markerOptions = new MarkerOptions();

                // Getting a place from the places list
                HashMap<String, String> hmPlace = list.get(i);

                // Getting latitude of the place
                double lat = Double.parseDouble(hmPlace.get("lat"));

                // Getting longitude of the place
                double lng = Double.parseDouble(hmPlace.get("lng"));
                LatLng latLng = new LatLng(lat, lng);
                String name = hmPlace.get("place_name");
                //Log.e("name",name);
                String vicinity = hmPlace.get("vicinity");
                String telpon = "02170640723";
                int temp = 1000000000;
                int index = 0;
                // Getting vicinity
                if (type.contains("puskesmas")) {
                    for (int a = 0; a < nama_puskesmas.size(); a++) {

                        if (minDistance(nama_puskesmas.get(a), name) < 20 && temp > minDistance(nama_puskesmas.get(a), name)) {
                            temp = minDistance(nama_puskesmas.get(a), name);
                            //vicinity = address.get(a);
                            vicinity = hmPlace.get("alamat");
                            if (telepon_puskesmas.get(a).length() > 2) {
                                telpon = telepon_puskesmas.get(a);
                            } else {
                                telpon = "";
                            }
                        } else {
                            vicinity = hmPlace.get("alamat");
                            telpon = "";
                        }
                    }

                } else if(type.contains("BPJS+Kesehatan+Jakarta")){


                }   else if(type.contains("hospital")) {
                    for (int a = 0; a < address.size(); a++) {

                        if (minDistance(data.get(a), name) < 20 && temp > minDistance(data.get(a), name)) {
                            temp = minDistance(data.get(a), name);
                            vicinity = address.get(a);
                            name = data.get(a);
                            if (telepon.get(a).length() > 2) {
                                telpon = telepon.get(a);
                            } else {
                                telpon = "";
                            }
                        } else {
                            vicinity = hmPlace.get("vicinity");
                            telpon = "";
                        }
                    }

                    int selectedPosition = mSprPlaceType.getSelectedItemPosition();
                    String type = mPlaceType[selectedPosition];

                    if (!type.contains("hospital")) {
                        vicinity = hmPlace.get("vicinity");
                    }else if(type.contains("puskesmas")&&type.contains("BPJS+Kesehatan+Jakarta")){
                        vicinity = hmPlace.get("alamat");
                    }
                }else{

                }

                Editor editor = sharedpreferences.edit();
                if (dist > distance(mLatitude, mLongitude, lat, lng)) {
                    dist = distance(mLatitude, mLongitude, lat, lng);
                    temp_lat = lat;
                    temp_long = lng;
                    editor.putString("telepon_dekat", telpon);
                    editor.commit();
                }

                //Log.e("Coba", String.valueOf(lat)+" "+String.valueOf(lng));

                // Setting the position for the marker
                markerOptions.position(latLng);

                // Setting the title for the marker.
                //This will be displayed on taping the marker
                markerOptions.title(name + " : " + vicinity);

                // Placing a marker on the touched position
                //mGoogleMap.addMarker(markerOptions);
                mMyMarkersArray.add(new MyMarker(name, "icon1", lat, lng));
                SPOT.add(new Spot(name, vicinity, telpon, latLng));


            }

            plotMarkers(SPOT);

        }

    }

    public double distance(double latA, double longA, double latB, double longB) {
        Location locationA = new Location("point A");
        locationA.setLatitude(latA);
        locationA.setLongitude(longA);
        Location locationB = new Location("point B");
        locationB.setLatitude(latB);
        locationB.setLongitude(longB);
        double distance = locationA.distanceTo(locationB);
        return distance;
    }

    public void handleGetDirectionsResult(ArrayList<LatLng> directionPoints) {

        PolylineOptions rectLine = new PolylineOptions().width(3).color(
                Color.RED);
        for (int i = 0; i < directionPoints.size(); i++) {
            rectLine.add(directionPoints.get(i));
        }
        map.addPolyline(rectLine);
    }

    @SuppressWarnings("unchecked")
    public void findDirections(double fromPositionDoubleLat,
                               double fromPositionDoubleLong, double toPositionDoubleLat,
                               double toPositionDoubleLong, String mode) {
        Map<String, String> map = new HashMap<String, String>();
        map.put(GetDirectionsAsyncTask.USER_CURRENT_LAT,
                String.valueOf(fromPositionDoubleLat));
        map.put(GetDirectionsAsyncTask.USER_CURRENT_LONG,
                String.valueOf(fromPositionDoubleLong));
        map.put(GetDirectionsAsyncTask.DESTINATION_LAT,
                String.valueOf(toPositionDoubleLat));
        map.put(GetDirectionsAsyncTask.DESTINATION_LONG,
                String.valueOf(toPositionDoubleLong));
        map.put(GetDirectionsAsyncTask.DIRECTIONS_MODE, mode);

        GetDirectionsAsyncTask asyncTask = new GetDirectionsAsyncTask(this);
        asyncTask.execute(map);
    }


    public static int minDistance(String word1, String word2) {
        int len1 = word1.length();
        int len2 = word2.length();

        // len1+1, len2+1, because finally return dp[len1][len2]
        int[][] dp = new int[len1 + 1][len2 + 1];

        for (int i = 0; i <= len1; i++) {
            dp[i][0] = i;
        }

        for (int j = 0; j <= len2; j++) {
            dp[0][j] = j;
        }

        //iterate though, and check last char
        for (int i = 0; i < len1; i++) {
            char c1 = word1.charAt(i);
            for (int j = 0; j < len2; j++) {
                char c2 = word2.charAt(j);

                //if last two chars equal
                if (c1 == c2) {
                    //update dp value for +1 length
                    dp[i + 1][j + 1] = dp[i][j];
                } else {
                    int replace = dp[i][j] + 1;
                    int insert = dp[i][j + 1] + 1;
                    int delete = dp[i + 1][j] + 1;

                    int min = replace > insert ? insert : replace;
                    min = delete > min ? min : delete;
                    dp[i + 1][j + 1] = min;
                }
            }
        }

        return dp[len1][len2];
    }

    private void plotMarkers(ArrayList<Spot> markers) {

        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.pin);
        map.clear();
        spots.clear();
        for (Spot spot : markers) {
            Marker marker = map.addMarker(new MarkerOptions()
                    .position(spot.getPosition())
                    .title("Title")
                    .snippet("Subtitle")
                    .icon(icon));

            spots.put(marker, spot);
        }

        findDirections(mLatitude, mLongitude, temp_lat, temp_long, GMapV2Direction.MODE_DRIVING);


        infoWindowContainer = rootView.findViewById(R.id.container_popup);
        //подписываемся на изменения размеров всплывающего окна
        infoWindowLayoutListener = new InfoWindowLayoutListener();
        infoWindowContainer.getViewTreeObserver().addOnGlobalLayoutListener(infoWindowLayoutListener);
        overlayLayoutParams = (AbsoluteLayout.LayoutParams) infoWindowContainer.getLayoutParams();

        alamat = (TextView) infoWindowContainer.findViewById(R.id.alamat);
        textView = (TextView) infoWindowContainer.findViewById(R.id.textview_title);
        button = (TextView) infoWindowContainer.findViewById(R.id.button_view_article);
        button.setOnClickListener(this);
        int selectedPosition = mSprPlaceType.getSelectedItemPosition();
        String type = mPlaceType[selectedPosition];
        if (!type.contains("hospital") && !type.contains("puskesmas")) {
            //alamat.setVisibility(INVISIBLE);
            button.setVisibility(INVISIBLE);
        } else {
            //alamat.setVisibility(VISIBLE);
            button.setVisibility(VISIBLE);
        }
    }

    private class PlacesTask extends AsyncTask<String, Integer, String> {

        String data = null;

        // Invoked by execute() method of this object
        @Override
        protected String doInBackground(String... url) {
            try {
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.e("Error", e.getLocalizedMessage());
            }
            return data;
        }

        // Executed after the complete execution of doInBackground() method
        @Override
        protected void onPostExecute(String result) {
            ParserTask parserTask = new ParserTask();

            // Start parsing the Google places in JSON format
            // Invokes the "doInBackground()" method of the class ParseTask
            parserTask.execute(result);
        }

    }


    @Override
    public void onLocationChanged(Location location) {
/*        map.animateCamera(CameraUpdateFactory.newLatLngZoom(
                new LatLng(location.getLatitude(), location.getLongitude()), 13));

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(location.getLatitude(), location.getLongitude()))      // Sets the center of the map to location user
                .zoom(17)                   // Sets the zoom
                        *//*.bearing(90)                // Sets the orientation of the camera to east
                        .tilt(40)  *//*                 // Sets the tilt of the camera to 30 degrees
                .build();                   // Creates a CameraPosition from the builder
        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));*/
        mLatitude = location.getLatitude();
        mLongitude = location.getLongitude();
        LatLng latLng = new LatLng(mLatitude, mLongitude);
        //Toast.makeText(getActivity().getBaseContext(),String.valueOf(mLatitude),Toast.LENGTH_LONG).show();
        map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        map.animateCamera(CameraUpdateFactory.zoomTo(12));

    }

    private class PositionUpdaterRunnable implements Runnable {
        private int lastXPosition = Integer.MIN_VALUE;
        private int lastYPosition = Integer.MIN_VALUE;

        @Override
        public void run() {
            //помещаем в очередь следующий цикл обновления
            handler.postDelayed(this, POPUP_POSITION_REFRESH_INTERVAL);

            //если всплывающее окно скрыто, ничего не делаем
            if (trackedPosition != null && infoWindowContainer.getVisibility() == VISIBLE) {
                Point targetPosition = getMap().getProjection().toScreenLocation(trackedPosition);

                //если положение окна не изменилось, ничего не делаем
                if (lastXPosition != targetPosition.x || lastYPosition != targetPosition.y) {
                    //обновляем положение
                    overlayLayoutParams.x = targetPosition.x - popupXOffset;
                    overlayLayoutParams.y = targetPosition.y - popupYOffset - markerHeight - 30;
                    infoWindowContainer.setLayoutParams(overlayLayoutParams);

                    //запоминаем текущие координаты
                    lastXPosition = targetPosition.x;
                    lastYPosition = targetPosition.y;
                }
            }
        }
    }

    public void asyncdata() {
        client.setTimeout(99999999);
        client.get("http://data.go.id/api/action/datastore_search?resource_id=59dcaaf4-5770-4098-aa22-9c74983787b9",
                new AsyncHttpResponseHandler() {
                    ProgressDialog nDialog = new ProgressDialog(getActivity());
                    AlertDialog.Builder aDialog = new AlertDialog.Builder(getActivity());
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                    @Override
                    public void onStart() {
                        // called before request is started
                        //showDialog(SURF_PROGRESS_BAR);
                        nDialog.setMessage("Tunggu Sebentar");
                        nDialog.setCancelable(false);
                        nDialog.show();
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers,
                                          byte[] response) {
                        nDialog.dismiss();
                            /*
                            aDialog.show();*/

                        String str = null;
                        try {

                            str = new String(response, "UTF-8");
                            JSONObject jsonObj = new JSONObject(str)
                                    .getJSONObject("result");

                            JSONArray jsonArray = jsonObj.getJSONArray("records");
                            //Log.e("JSON",jsonArray.getJSONObject(0).getString("NAMA  RUMAH SAKIT"));
                            for (int i = 0; i < jsonArray.length(); i++) {
                                data.add(jsonArray.getJSONObject(i).getString("NAMA  RUMAH SAKIT"));
                                address.add(jsonArray.getJSONObject(i).getString("ALAMAT LOKASI  RUMAH SAKIT"));
                                if (0 < jsonArray.getJSONObject(i).getString("NOMOR TELEPON").length()) {
                                    telepon.add("021" + jsonArray.getJSONObject(i).getString("NOMOR TELEPON").replace("-", "").replace(" ", "").replace(".0", "").replace(",", "").substring(0, 7));
                                } else {
                                    telepon.add("");
                                }

                                //Log.e("JSON",telepon.get(i));
                            }
                            //Log.d("JSON",/*jsonArray.getJSONObject(0).getString("NAMA RUMAH SAKIT")*/"asas");
                            /*SharedPreferences.Editor editor = sharedpreferences.edit();
                            editor.putString("telp", telepon.get(1));
                            editor.commit();*/
                        } catch (UnsupportedEncodingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                            Toast.makeText(getActivity().getBaseContext(),
                                    "EncodingException", Toast.LENGTH_LONG)
                                    .show();
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                            Toast.makeText(getActivity().getBaseContext(),
                                    e.getLocalizedMessage(), Toast.LENGTH_LONG)
                                    .show();
                        }

                        /*try {
                            Log.e("JSON",new String(response, "UTF-8"));
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }*/

                        //dismissDialog(SURF_PROGRESS_BAR);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers,
                                          byte[] errorResponse, Throwable e) {
                        // called when response HTTP status is "4XX"
                        // (eg. 401, 403, 404)
                        nDialog.dismiss();
                        builder.setMessage("Koneksi Internet Bermasalah");
                        builder.setPositiveButton("Coba Lagi", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                asyncdata();
                            }
                        });
                        AlertDialog dialog = builder.create();
                        dialog.show();
                        TextView messageText = (TextView) dialog.findViewById(android.R.id.message);
                        messageText.setGravity(Gravity.CENTER);
                        aDialog.show();
                        //showDialog(SURF_INTERUPTED);

                    }

                });

    }

    ArrayList<String> nama_puskesmas = new ArrayList<String>();
    ArrayList<String> alamat_puskesmas = new ArrayList<String>();
    ArrayList<String> telepon_puskesmas = new ArrayList<String>();


    public void readExcelFileFromAssets() {

        try {
            // Creating Input Stream
            /*
             * File file = new File( filename); FileInputStream myInput = new
			 * FileInputStream(file);
			 */

            InputStream myInput;

            myInput = new FileInputStream(ExternalStorageDirectoryPath + "/data.xls");
            //InputgetActivity().getAssets().open("contacts.xls");

            // Create a POIFSFileSystem object
            POIFSFileSystem myFileSystem = new POIFSFileSystem(myInput);

            // Create a workbook using the File System
            HSSFWorkbook myWorkBook = new HSSFWorkbook(myFileSystem);

            // Get the first sheet from workbook
            HSSFSheet mySheet = myWorkBook.getSheetAt(0);

            /** We now need something to iterate through the cells. **/
            Iterator<Row> rowIter = mySheet.rowIterator();
            nama_puskesmas.clear();
            alamat_puskesmas.clear();
            telepon_puskesmas.clear();
            while (rowIter.hasNext()) {
                HSSFRow myRow = (HSSFRow) rowIter.next();
                Log.e("FileUtils", "Cell Value: " + myRow.getCell(3).toString() + " Index :" + myRow.getRowNum());
                nama_puskesmas.add(myRow.getCell(3).toString());
                alamat_puskesmas.add(myRow.getCell(5).toString().replace(" ", ""));
                telepon_puskesmas.add(myRow.getCell(6).toString());
                /*Iterator<Cell> cellIter = myRow.cellIterator();
                //Log.e("File",cellIter.toString());
                while (cellIter.hasNext()) {
                    HSSFCell myCell = (HSSFCell) cellIter.next();
                    //Log.e("FileUtils", "Cell Value: " + myCell.toString()+ " Index :" +myCell.getColumnIndex());
                    // Toast.makeText(getApplicationContext(), "cell Value: " +
                    // myCell.toString(), Toast.LENGTH_SHORT).show();
                }*/
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return;
    }

    String ExternalStorageDirectoryPath = Environment
            .getExternalStorageDirectory().getAbsolutePath();

    private class DownloadTask extends AsyncTask<String, Integer, String> {

        private Context context;
        private PowerManager.WakeLock mWakeLock;

        public DownloadTask(Context context) {
            this.context = context;
        }

        @Override
        protected String doInBackground(String... sUrl) {
            InputStream input = null;
            OutputStream output = null;
            HttpURLConnection connection = null;
            try {
                URL url = new URL("https://simpan.ugm.ac.id/public.php?service=files&t=edd50708830f2f6ffd8de0fbad896248&download");
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                // expect HTTP 200 OK, so we don't mistakenly save error report
                // instead of the file
                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    //Toast.makeText(context,"eekmu bau",Toast.LENGTH_SHORT).show();
                    Log.e("HAHA", "Server returned HTTP " + connection.getResponseCode()
                            + " " + connection.getResponseMessage());
                    return "Server returned HTTP " + connection.getResponseCode()
                            + " " + connection.getResponseMessage();

                }

                // this will be useful to display download percentage
                // might be -1: server did not report the length
                int fileLength = connection.getContentLength();

                // download the file
                input = connection.getInputStream();
                output = new FileOutputStream(ExternalStorageDirectoryPath + "/data.xls");

                byte data[] = new byte[4096];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    // allow canceling with back button
                    if (isCancelled()) {
                        input.close();
                        Log.e("EOK", "GIMANA");
                        //Toast.makeText(context,"eekmu bau",Toast.LENGTH_SHORT).show();
                        return null;
                    }
                    total += count;
                    // publishing the progress....
                    if (fileLength > 0) // only if total length is known
                        publishProgress((int) (total * 100 / fileLength));
                    output.write(data, 0, count);
                }
            } catch (Exception e) {
                //Toast.makeText(context,e.toString(),Toast.LENGTH_SHORT).show();
                Log.e("HAHAHA", e.getLocalizedMessage());
                return e.toString();

            } finally {
                try {
                    if (output != null)
                        output.close();
                    if (input != null)
                        input.close();
                } catch (IOException ignored) {
                    Toast.makeText(context, "eekmu bau", Toast.LENGTH_SHORT).show();
                }

                if (connection != null)
                    connection.disconnect();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {

            readExcelFileFromAssets();

        }

    }
}
