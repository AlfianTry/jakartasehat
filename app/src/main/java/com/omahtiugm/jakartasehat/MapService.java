package com.omahtiugm.jakartasehat;

import android.app.Dialog;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Microplex on 3/28/2015.
 */
public class MapService extends Service implements LocationProvider.LocationCallback{

    public static final String MyPREFERENCES = "MyPrefs";
    SharedPreferences sharedpreferences;
    AsyncHttpClient client = new AsyncHttpClient();
    private ArrayList<String> data = new ArrayList<String>();
    private ArrayList<String> address = new ArrayList<String>();
    private ArrayList<String> telepon = new ArrayList<String>();
    private double mLatitude;
    private double mLongitude;
    private LocationProvider mLocationProvider;
    private double temp_lat = 0;
    private double temp_long = 0;
    private static Handler handler;


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sharedpreferences = getSharedPreferences(MyPREFERENCES,
                Context.MODE_PRIVATE);
        asyncdata();
        mLocationProvider = new LocationProvider(this, this);
        mLocationProvider.connect();
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());


        if(status!= ConnectionResult.SUCCESS){ // Google Play Services are not available

            /*int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, getActivity(), requestCode);
            dialog.show();*/

        }else {

            // Get LocationManager object from System Service LOCATION_SERVICE
            LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

            // Create a criteria object to retrieve provider
            Criteria criteria = new Criteria();

            // Get the name of the best provider
            String provider = locationManager.getBestProvider(criteria, true);

            // Get Current Location
            Location myLocation = locationManager.getLastKnownLocation(provider);


            // Get latitude of the current location
            double latitude = myLocation.getLatitude();

            // Get longitude of the current location
            double longitude = myLocation.getLongitude();

            // Create a LatLng object for the current location
            LatLng latLng = new LatLng(latitude, longitude);
            mLatitude = latLng.latitude;
            mLongitude = latLng.longitude;
            StringBuilder sb = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
            sb.append("location="+mLatitude+","+mLongitude);
            sb.append("&radius=5000");
            sb.append("&types=hospital");
            //sb.append("&sensor=true");
            sb.append("&key=AIzaSyDelgkhRmsbqBg14ypPVK7sfRhLeejqVIY");

            /*String a = "asas";
            a.compareTo("asas");*/

            // Creating a new non-ui thread task to download Google place json data
            PlacesTask placesTask = new PlacesTask();

            // Invokes the "doInBackground()" method of the class PlaceTask
            placesTask.execute(sb.toString());

        }



    }

    @Override
    public void onStart(Intent intent, int startId) {
        // TODO Auto-generated method stub
        super.onStart(intent, startId);
        //Toast.makeText(this, "Service running", Toast.LENGTH_SHORT).show();

        handler = new Handler(){

            @Override
            public void handleMessage(Message msg) {
                // TODO Auto-generated method stub
                super.handleMessage(msg);
                mLocationProvider.connect();
                StringBuilder sb = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
                sb.append("location="+mLatitude+","+mLongitude);
                sb.append("&radius=5000");
                sb.append("&types=hospital");
                //sb.append("&sensor=true");
                sb.append("&key=AIzaSyA85niT7OmYt-XB_NsGTh_7G41HzGRuT_U");

            /*String a = "asas";
            a.compareTo("asas");*/

                // Creating a new non-ui thread task to download Google place json data
                PlacesTask placesTask = new PlacesTask();

                // Invokes the "doInBackground()" method of the class PlaceTask
                placesTask.execute(sb.toString());
                //Toast.makeText(MapService.this, "5 secs has passed", Toast.LENGTH_SHORT).show();
            }

        };



        new Thread(new Runnable(){
            public void run() {
                // TODO Auto-generated method stub
                while(true)
                {
                    try {
                        Thread.sleep(50000);
                        handler.sendEmptyMessage(0);

                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                }

            }
        }).start();
    }

    public void asyncdata() {
        client.setTimeout(99999999);
        client.get("http://data.go.id/api/action/datastore_search?resource_id=59dcaaf4-5770-4098-aa22-9c74983787b9",
                new AsyncHttpResponseHandler() {

                    @Override
                    public void onStart() {
                        //Toast.makeText(context, "mulai", Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers,
                                          byte[] response) {
                            /*
                            aDialog.show();*/

                        String str = null;
                        try {

                            str = new String(response, "UTF-8");
                            JSONObject jsonObj = new JSONObject(str)
                                    .getJSONObject("result");

                            JSONArray jsonArray = jsonObj.getJSONArray("records");
                            Log.e("JSON", jsonArray.getJSONObject(0).getString("NAMA  RUMAH SAKIT"));
                            for (int i = 0; i < jsonArray.length(); i++) {
                                data.add(jsonArray.getJSONObject(i).getString("NAMA  RUMAH SAKIT"));
                                address.add(jsonArray.getJSONObject(i).getString("ALAMAT LOKASI  RUMAH SAKIT"));
                                if(0<jsonArray.getJSONObject(i).getString("NOMOR TELEPON").length()){
                                    telepon.add("021"+jsonArray.getJSONObject(i).getString("NOMOR TELEPON").replace("-","").replace(" ","").replace(".0","").replace(",","").substring(0,7));
                                }else{
                                    telepon.add("");
                                }

                                Log.e("JSON",telepon.get(i));
                            }
                            //Log.d("JSON",/*jsonArray.getJSONObject(0).getString("NAMA RUMAH SAKIT")*/"asas");

                        } catch (UnsupportedEncodingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString("telp", telepon.get(1));
                        editor.commit();

                        /*try {
                            Log.e("JSON",new String(response, "UTF-8"));
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }*/
                        //telp = telepon.get(1);
                        //Toast.makeText(context, "berhasil", Toast.LENGTH_SHORT).show();
                        //dismissDialog(SURF_PROGRESS_BAR);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers,
                                          byte[] errorResponse, Throwable e) {
                        // called when response HTTP status is "4XX"
                        // (eg. 401, 403, 404)



                    }

                });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Cancel the persistent notification.

    }

    public static void updateMyWidgets(Context context, String data) {
        AppWidgetManager man = AppWidgetManager.getInstance(context);
        int[] ids = man.getAppWidgetIds(
                new ComponentName(context,AmbulanceWidget.class));
        Intent updateIntent = new Intent();
        updateIntent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        updateIntent.putExtra(AmbulanceWidget.WIDGET_IDS_KEY, ids);
        updateIntent.putExtra(AmbulanceWidget.WIDGET_DATA_KEY, data);
        context.sendBroadcast(updateIntent);
    }

    public double distance(double latA, double longA, double latB, double longB) {
        Location locationA = new Location("point A");
        locationA.setLatitude(latA);
        locationA.setLongitude(longA);
        Location locationB = new Location("point B");
        locationB.setLatitude(latB);
        locationB.setLongitude(longB);
        double distance = locationA.distanceTo(locationB);
        return distance;
    }

    @Override
    public void handleNewLocation(Location location) {
        Log.d("TAG", location.toString());

        double currentLatitude = location.getLatitude();
        double currentLongitude = location.getLongitude();
        mLatitude = currentLatitude;
        mLongitude = currentLongitude;
        LatLng latLng = new LatLng(currentLatitude, currentLongitude);

    }

    private class PlacesTask extends AsyncTask<String, Integer, String> {

        String data = null;

        // Invoked by execute() method of this object
        @Override
        protected String doInBackground(String... url) {
            try{
                data = downloadUrl(url[0]);
            }catch(Exception e){
                Log.e("Error", e.getLocalizedMessage());
            }
            return data;
        }

        // Executed after the complete execution of doInBackground() method
        @Override
        protected void onPostExecute(String result){
            ParserTask parserTask = new ParserTask();

            // Start parsing the Google places in JSON format
            // Invokes the "doInBackground()" method of the class ParseTask
            parserTask.execute(result);
        }

    }

    private class ParserTask extends AsyncTask<String, Integer, List<HashMap<String,String>>> {

        JSONObject jObject;

        // Invoked by execute() method of this object
        @Override
        protected List<HashMap<String,String>> doInBackground(String... jsonData) {

            List<HashMap<String, String>> places = null;

            PlaceJSONParser placeJsonParser = new PlaceJSONParser("0");

            try{
                jObject = new JSONObject(jsonData[0]);

                /** Getting the parsed data as a List construct */
                places = placeJsonParser.parse(jObject);

            }catch(Exception e){
                Log.e("Error", e.getLocalizedMessage());
            }
            return places;
        }

        // Executed after the complete execution of doInBackground() method
        @Override
        protected void onPostExecute(List<HashMap<String,String>> list){

            // Clears all the existing markers
            double dist = 999999999;

            for(int i=0;i<list.size();i++){

                // Creating a marker
                MarkerOptions markerOptions = new MarkerOptions();

                // Getting a place from the places list
                HashMap<String, String> hmPlace = list.get(i);

                // Getting latitude of the place
                double lat = Double.parseDouble(hmPlace.get("lat"));

                // Getting longitude of the place
                double lng = Double.parseDouble(hmPlace.get("lng"));


                // Getting name
                String name = hmPlace.get("place_name");
                String telpon = "02170640723";
                int temp = 1000000000;
                // Getting vicinity
                for(int a=0;a<address.size();a++){

                    if(minDistance(data.get(a),name)<20&&temp>minDistance(data.get(a),name)) {
                        temp = minDistance(data.get(a), name);
                        if(telepon.get(a).length()>2) {
                            telpon = telepon.get(a);
                        }else{
                            telpon = "";
                        }
                    }else{
                        telpon = "";
                    }
                }

                SharedPreferences.Editor editor = sharedpreferences.edit();
                if(dist>distance(mLatitude,mLongitude,lat,lng)){
                    dist = distance(mLatitude,mLongitude,lat,lng);
                    temp_lat = lat;
                    temp_long = lng;
                    editor.putString("telepon_dekat",telpon);
                    editor.commit();
                    updateMyWidgets(getBaseContext(),telpon);
                }



                LatLng latLng = new LatLng(lat, lng);


            }


        }

    }

    public static int minDistance(String word1, String word2) {
        int len1 = word1.length();
        int len2 = word2.length();

        // len1+1, len2+1, because finally return dp[len1][len2]
        int[][] dp = new int[len1 + 1][len2 + 1];

        for (int i = 0; i <= len1; i++) {
            dp[i][0] = i;
        }

        for (int j = 0; j <= len2; j++) {
            dp[0][j] = j;
        }

        //iterate though, and check last char
        for (int i = 0; i < len1; i++) {
            char c1 = word1.charAt(i);
            for (int j = 0; j < len2; j++) {
                char c2 = word2.charAt(j);

                //if last two chars equal
                if (c1 == c2) {
                    //update dp value for +1 length
                    dp[i + 1][j + 1] = dp[i][j];
                } else {
                    int replace = dp[i][j] + 1;
                    int insert = dp[i][j + 1] + 1;
                    int delete = dp[i + 1][j] + 1;

                    int min = replace > insert ? insert : replace;
                    min = delete > min ? min : delete;
                    dp[i + 1][j + 1] = min;
                }
            }
        }

        return dp[len1][len2];
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);


            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb  = new StringBuffer();

            String line = "";
            while( ( line = br.readLine())  != null){
                sb.append(line);
                Log.e("Bisa",line);
            }

            data = sb.toString();

            br.close();

        }catch(Exception e){
            Log.e("Error",e.getLocalizedMessage());
        }finally{
            Log.e("Error","Error Download URL");
            iStream.close();
            urlConnection.disconnect();
        }

        return data;
    }
}
