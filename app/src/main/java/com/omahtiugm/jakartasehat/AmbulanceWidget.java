package com.omahtiugm.jakartasehat;


import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.util.Log;
import android.view.Gravity;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class AmbulanceWidget extends AppWidgetProvider {

    public static final String MyPREFERENCES = "MyPrefs";
    SharedPreferences sharedpreferences;
    public static final String WIDGET_IDS_KEY ="mywidgetproviderwidgetids";
    public static final String WIDGET_DATA_KEY ="mywidgetproviderwidgetdata";
    RemoteViews views;
    Context context;
    String telp = "";
    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager,
                         int[] appWidgetIds) {
        this.context = context;


        context.startService(new Intent(context, MapService.class));
        super.onUpdate(context, appWidgetManager, appWidgetIds);
        sharedpreferences = context.getSharedPreferences(MyPREFERENCES,
                Context.MODE_PRIVATE);
        telp = sharedpreferences.getString("telp","118");
        update(context, appWidgetManager, appWidgetIds, null);
        final int N = appWidgetIds.length;

        // Perform this loop procedure for each App Widget that belongs to this provider
        for (int i=0; i<N; i++) {

            int appWidgetId = appWidgetIds[i];
            context.startService(new Intent(context, MapService.class));
            sharedpreferences = context.getSharedPreferences(MyPREFERENCES,
                    Context.MODE_PRIVATE);
            telp = sharedpreferences.getString("telp","118");
            // Create an Intent to launch MainActivity
            Intent callIntent = new Intent(Intent.ACTION_CALL);


            callIntent.setData(Uri.parse("tel:"+telp));

            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, callIntent,PendingIntent.FLAG_UPDATE_CURRENT);

            // Get the layout for the App Widget
            views = new RemoteViews(context.getPackageName(), R.layout.widget);

            //  Attach an on-click listener to the clock
            views.setOnClickPendingIntent(R.id.button2, pendingIntent);

            // Tell the AppWidgetManager to perform an update on the current app widget
            appWidgetManager.updateAppWidget(appWidgetId, views);

        }



    }

    public void update(Context context, AppWidgetManager manager, int[] ids, String data) {

        //data will contain some predetermined data, but it may be null
        for (int widgetId : ids) {
            sharedpreferences = context.getSharedPreferences(MyPREFERENCES,
                    Context.MODE_PRIVATE);
            telp = data;
            Intent callIntent = new Intent(Intent.ACTION_CALL);


            callIntent.setData(Uri.parse("tel:"+telp));

            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, callIntent,PendingIntent.FLAG_UPDATE_CURRENT);

            // Get the layout for the App Widget
            views = new RemoteViews(context.getPackageName(), R.layout.widget);

            //  Attach an on-click listener to the clock
            views.setOnClickPendingIntent(R.id.button2, pendingIntent);

            // Tell the AppWidgetManager to perform an update on the current app widget
            manager.updateAppWidget(widgetId, views);
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.hasExtra(WIDGET_IDS_KEY)) {
            int[] ids = intent.getExtras().getIntArray(WIDGET_IDS_KEY);
            if (intent.hasExtra(WIDGET_DATA_KEY)) {
                sharedpreferences = context.getSharedPreferences(MyPREFERENCES,
                        Context.MODE_PRIVATE);
                telp = sharedpreferences.getString("telp","118");
                String data = intent.getStringExtra(WIDGET_DATA_KEY);
                this.update(context, AppWidgetManager.getInstance(context), ids, data);
            } else {
                this.onUpdate(context, AppWidgetManager.getInstance(context), ids);
            }
        } else super.onReceive(context, intent);
    }



}
