package com.omahtiugm.jakartasehat;

import com.google.android.gms.maps.model.LatLng;

public class Spot {
    private String name;
    private String address;
    private LatLng position;
    private String telpon;

    public Spot(String name, String address, String telpon, LatLng position) {
        this.name = name;
        this.address = address;
        this.telpon = telpon;
        this.position = position;
    }

    public String getName() {
        return name;
    }

    public String getAddress(){
        return address;
    }

    public LatLng getPosition() {
        return position;
    }

    public String getTelpon(){
        return telpon;
    }
}
