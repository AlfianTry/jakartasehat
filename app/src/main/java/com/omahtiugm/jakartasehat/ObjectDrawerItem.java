package com.omahtiugm.jakartasehat;

/**
 * Created by Microplex on 3/28/2015.
 */
public class ObjectDrawerItem {

    public int icon;
    public String name;

    // Constructor.
    public ObjectDrawerItem(int icon, String name) {

        this.icon = icon;
        this.name = name;
    }
}
